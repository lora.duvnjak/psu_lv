import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

new_weight = mtcars.wt * 1000 * 0.4549
mtcars.wt = new_weight
print(mtcars.wt)