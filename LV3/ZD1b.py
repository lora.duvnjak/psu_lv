import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv') 

mtcars.sort_values(by= ['mpg'], inplace = True)

mt = mtcars[(mtcars.cyl == 8)]

print(mt.head(3))