import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

print(mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].car)
print("Automatski auti s više od 100 konjskih snaga: ",len(mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].car))