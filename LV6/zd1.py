import numpy as np
from sklearn.datasets import fetch_openml
import joblib
import pickle
import matplotlib.pyplot as plt
import matplotlib.image as pltimg
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix 
from sklearn.metrics import plot_confusion_matrix


X, y = fetch_openml('mnist_784', version=1, return_X_y=True, as_frame=False)


# TODO: prikazi nekoliko ulaznih slika
for i in range(5):

   plt.imshow(X[i].reshape(28,28), cmap='gray')
   plt.show()

# skaliraj podatke, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]



# TODO: izgradite vlastitu mrezu pomocu sckitlearn MPLClassifier 
mlp_mnist = MLPClassifier(random_state=1, max_iter=300,verbose=True).fit(X_train, y_train)

# TODO: evaluirajte izgradenu mrezu

print("Train set score: ", mlp_mnist.score(X_train, y_train))
print("Test set score: ", mlp_mnist.score(X_test,y_test))

matrix = confusion_matrix(mlp_mnist.predict(X_test), y_test)
print(matrix)
#a = plot_confusion_matrix(matrix)
#plt.show()

# spremi mrezu na disk
filename = "NN_model.sav"
joblib.dump(mlp_mnist, filename)