import scipy as sp
from sklearn import cluster, datasets 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg 

try:
    face = sp.face(gray=True)
except AttributeError:
    from scipy import misc
    face = misc.face(gray=True)

X = face.reshape((-1, 1))  (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=5,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_face_compressed = np.choose(labels, values)face_compressed.shape = face.shape

plt.figure(1)
plt.imshow(face,  cmap='gray')

plt.figure(2)
plt.imshow(face_compressed,  cmap='gray')

imageNew = mpimg.imread('example_grayscale.png')
plt.imshow = imageNew 
